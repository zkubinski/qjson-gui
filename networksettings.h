#ifndef NETWORK_H
#define NETWORK_H

#include <QObject>
#include <QJsonObject>

class NetworkSettings : public QObject
{
    Q_OBJECT
public:
    explicit NetworkSettings(QObject *parent = nullptr); //zwykły konstruktor
    NetworkSettings(NetworkSettings &sourceObject); //konstruktor kopiujący
    NetworkSettings &operator=(NetworkSettings &sourceObject); //kopiujący operator przypisania

    void setNetworkData(QJsonObject &_network);
    QJsonObject &NetworkData();

    QString IPAddress();
    QString ConnectType();

public slots:
    void setIPAddress(const QString &_ipAddress);
    void setConnectType(const QString &_connectType);

private:
    QJsonObject createNetworkSettings;
    QString ipv4Address, connectionType;

signals:

};

#endif // NETWORK_H
