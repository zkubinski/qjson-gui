#include "filesettings.h"

FileSettings::FileSettings(QObject *parent) : QObject(parent)
{

}

void FileSettings::setFileToSaveSettings(QJsonObject &JsonObj)
{
    nJsObject = JsonObj;
}

void FileSettings::SaveSettings()
{
    QJsonDocument nJsDocument(nJsObject);

    SaveFile.setFileName("settings.json");

    if(!SaveFile.open(QIODevice::WriteOnly | QIODevice::Text | QIODevice::Truncate)){
        qDebug()<< "blad otwarcia bufora pliku";
    }
    else{
        SaveFile.write(nJsDocument.toJson());
        SaveFile.flush();
        SaveFile.close();
    }
}
