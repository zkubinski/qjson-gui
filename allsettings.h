#ifndef ALLSETTINGS_H
#define ALLSETTINGS_H

#include <QObject>
#include <QJsonObject>

#include "networksettings.h"

class AllSettings : public QObject
{
    Q_OBJECT
public:
    explicit AllSettings(QObject *parent = nullptr);

    QJsonObject &setAllSettings(QJsonObject &_settings);

public slots:
    void setNetworkIPAddress(const QString &ipAddress);

    void ShowSettings();

signals:

private:
    QJsonObject createSettings;
    NetworkSettings createNetworkSettings;
};

#endif // ALLSETTINGS_H
