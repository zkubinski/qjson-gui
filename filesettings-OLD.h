#ifndef FILESETTINGS_H
#define FILESETTINGS_H

#include <QObject>
#include <QJsonObject>
#include <QJsonDocument>
#include <QFile>

class FileSettings : public QObject
{
    Q_OBJECT

public:
    explicit FileSettings(QObject *parent = nullptr);

    void setFileToSaveSettings(QJsonObject &);

public slots:
    void SaveSettings();

private:
    QJsonObject nJsObject;
    QFile SaveFile;
};

#endif // FILESETTINGS_H
