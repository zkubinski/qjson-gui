#include "allsettings.h"
#include <QDebug>

AllSettings::AllSettings(QObject *parent) : QObject(parent)
{
}

QJsonObject &AllSettings::setAllSettings(QJsonObject &_Settings)
{
    QJsonObject NetworkSettings;

    NetworkSettings["network"] = createNetworkSettings.setNetworkData(_Settings);

    createSettings = NetworkSettings;

    return createSettings;
}

void AllSettings::setNetworkIPAddress(const QString &IPAddress)
{
    createNetworkSettings.setIPAddress(IPAddress);

    createSettings.insert("ipaddr", createNetworkSettings.IPAddress());
}

void AllSettings::ShowSettings()
{
    QJsonObject::iterator i;

    for(i = createSettings.begin(); i != createSettings.end(); ++i){
        qDebug()<< i.key() << i.value().toString();
    }
}
