#include "networksettings.h"
#include <QDebug>

NetworkSettings::NetworkSettings(QObject *parent) : QObject(parent), ipv4Address("127.0.0.1"), connectionType("localhost")
{

}

NetworkSettings::NetworkSettings(NetworkSettings &sourceObject)
{
    ipv4Address = sourceObject.ipv4Address;
    connectionType = sourceObject.connectionType;
}

NetworkSettings &NetworkSettings::operator=(NetworkSettings &sourceObject)
{
    ipv4Address = sourceObject.ipv4Address;
    connectionType = sourceObject.connectionType;

    return *this;
}

void NetworkSettings::setNetworkData(QJsonObject &_Network)
{
    _Network["ipaddr"] = ipv4Address;
    _Network["connect"] = connectionType;

    createNetworkSettings = _Network;
}

QJsonObject &NetworkSettings::NetworkData()
{
    return createNetworkSettings;
}

void NetworkSettings::setIPAddress(const QString &_IPAddress)
{
    ipv4Address = _IPAddress;

    QJsonObject::iterator i;

    for(i = createNetworkSettings.begin(); i != createNetworkSettings.end(); ++i){
        if(i.key() == "ipaddr"){
            createNetworkSettings.insert(i.key(), ipv4Address);
        }
    }

    qDebug()<< "value" << createNetworkSettings.value("ipaddr").toString();
}

void NetworkSettings::setConnectType(const QString &_ConnectType)
{

}

QString NetworkSettings::IPAddress()
{
    return ipv4Address;
}

QString NetworkSettings::ConnectType()
{
    return connectionType;
}
