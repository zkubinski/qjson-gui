#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QWidget>
#include <QPushButton>
#include <QLineEdit>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QFile>

#include "allsettings.h"
#include "fileconfig.h"

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private:
    QWidget *mainWidget;
    QVBoxLayout *mainLayout;
    QHBoxLayout *layout;
    QLineEdit *lineEdit;
    QPushButton *pbSave, *pbExit;

//    QJsonDocument *JsDoc;
    QJsonObject createSettings;
    AllSettings createAllSettings;
    FileConfig createFileConfig;
};
#endif // MAINWINDOW_H
