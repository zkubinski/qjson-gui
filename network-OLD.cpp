#include "network.h"

Network::Network() : IPAddr("127.0.0.1")
{

}

void Network::setNetworkSettings(QJsonObject &JsObject)
{
    JsObject["ipaddr"] = IPAddr;

    nJsObject = JsObject;
}

QJsonObject Network::NetworkSettings(){
    return nJsObject;
}

void Network::setIPAddress(const QString &ipv4){
    IPAddr = ipv4;

    QJsonObject::iterator i;

    for(i = nJsObject.begin(); i != nJsObject.end(); ++i){
        if(i.key() == "ipaddr"){
            nJsObject.insert(i.key(), IPAddr);
        }
    }
}
