#ifndef NETWORK_H
#define NETWORK_H

#include <QObject>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonValue>
#include <QString>

#include <QDebug>

class Network : public QObject
{
    Q_OBJECT

public:
    Network();

    QString IPAddress(){
        return IPAddr;
    }

    void setNetworkSettings(QJsonObject &JsObject);

    QJsonObject NetworkSettings();

public slots:
    void setIPAddress(const QString &ipv4);

private:
    QString IPAddr;
    QJsonObject nJsObject;
};

#endif // NETWORK_H
