#include "settings.h"

Settings::Settings()
{

}

void Settings::CreateSettings(QJsonObject &Object){
    QJsonObject JsonObj, MainJsonObj;

    networkSettings.setNetworkSettings(Object);
    JsonObj["network"] = networkSettings.NetworkSettings();

    for(QString &str : JsonObj.keys()){
        MainJsonObj.insert(str, JsonObj);
    }
}

void Settings::setIPNetworkSettings(const QString &ip){
    networkSettings.setIPAddress(ip);
}
