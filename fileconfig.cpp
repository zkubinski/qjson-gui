#include "fileconfig.h"
#include <QDebug>

FileConfig::FileConfig(QObject *parent) : QObject(parent)
{

}

QJsonObject FileConfig::setConfigToSave(QJsonObject &settings)
{
    getsSettings = storesSettings.setAllSettings(settings);

    return getsSettings;
}

void FileConfig::ShowConfig()
{
    QJsonObject::iterator i;

    for(i = getsSettings.begin(); i != getsSettings.end(); ++i){
        qDebug()<< i.key() << i.value().toString();
    }
}

void FileConfig::SaveConfig()
{
    QJsonDocument createDocumentSettings(getsSettings);

    createFileConfig.setFileName(QString("settings.json"));

    if(!createFileConfig.open(QIODevice::WriteOnly | QIODevice::Text | QIODevice::Truncate)){
        qDebug()<< "Nie mozna utworzyc bufora pliku";
    }
    else{
        createFileConfig.write(createDocumentSettings.toJson());
        createFileConfig.flush();
        createFileConfig.close();

        qDebug()<< "Plik z ustawieniami zostal zapisany";

        ShowConfig();
    }
}

void FileConfig::ReadConfig()
{

}
