#include "mainwindow.h"
#include <QDebug>

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent)
{
    resize(300,200);

    mainWidget = new QWidget(this);

    mainLayout = new QVBoxLayout(mainWidget);

    lineEdit = new QLineEdit(this);
//    LineEdit->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);

    pbSave = new QPushButton(this);
    pbSave->setText(QString(tr("Zapisz")));

    pbExit = new QPushButton(this);
    pbExit->setText(QString(tr("Zamknij")));

    layout = new QHBoxLayout();
    layout->addWidget(pbSave);
    layout->addWidget(pbExit);

    mainLayout->addWidget(lineEdit);
    mainLayout->addLayout(layout);

    setCentralWidget(mainWidget);

    createAllSettings.setAllSettings(createSettings);
    createFileConfig.setConfigToSave(createSettings);

    QObject::connect(lineEdit, &QLineEdit::textEdited, &createAllSettings, &AllSettings::setNetworkIPAddress);
    QObject::connect(pbSave, &QPushButton::clicked, &createFileConfig, &FileConfig::SaveConfig);
    QObject::connect(pbExit, &QPushButton::clicked, this, &MainWindow::close);
}

MainWindow::~MainWindow()
{
}

