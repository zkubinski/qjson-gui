#ifndef SETTINGS_H
#define SETTINGS_H

#include <QObject>
#include <QJsonObject>

#include "network.h"

class Settings : public QObject
{
    Q_OBJECT

public:
    Settings();

    void CreateSettings(QJsonObject &Object);

    void setIPNetworkSettings(const QString &ip);

private:
    Network networkSettings;
    QJsonObject JsObject;
};

#endif // SETTINGS_H
