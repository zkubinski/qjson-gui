#ifndef FILECONFIG_H
#define FILECONFIG_H

#include <QObject>
#include <QJsonObject>
#include <QJsonDocument>
#include <QFile>

#include "allsettings.h"

class FileConfig : public QObject
{
    Q_OBJECT
public:
    explicit FileConfig(QObject *parent = nullptr);

    QJsonObject setConfigToSave(QJsonObject &settings);

public slots:
    void ShowConfig();
    void SaveConfig();
    void ReadConfig();

private:
    AllSettings storesSettings;
    QJsonObject getsSettings;
    QFile createFileConfig;

signals:

};

#endif // FILECONFIG_H
